# Messaging
Ability to send and receive SMS, MMS

* browser: **partial** send with dedicated URL schemes; **alternative** via on-line services
* sysapp: **yes** [Messaging API](http://www.w3.org/2012/sysapps/messaging/)
* iOS: **partial** [Delegated to system app via `MFMessageComposeViewController`](https://developer.apple.com/library/ios/documentation/MessageUI/Reference/MFMessageComposeViewController_class/Reference/Reference.html)
* Android: **yes** [SmsManager](http://developer.android.com/reference/android/telephony/SmsManager.html)
* Windows 8: **yes** [Windows.Devices.Sms](http://code.msdn.microsoft.com/windowsapps/Sms-SendReceive-fa02e55e)
* Phonegap: **partial** [SMS Plugin](https://github.com/aharris88/phonegap-sms-plugin)
* FirefoxOS: **partial** [WebSMS](https://developer.mozilla.org/en-US/docs/Web/API/WebSMS_API) reserved to certified apps
* ChromeApps: **no**
* Tizen: **yes** [Messaging API](https://developer.tizen.org/dev-guide/2.2.0/org.tizen.web.device.apireference/tizen/messaging.html)

