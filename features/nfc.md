# NFC
Read / write access to NFC-enabled devices

* browser: **no**
* sysapp: **yes** [NFC API](http://www.w3.org/2012/nfc/web-api/)
* iOS: **no** (maybe coming up?)
* Android: **yes** [`android.nfc.NfcAdapter`](http://developer.android.com/reference/android/nfc/package-summary.html)
* Windows 8: **yes** [`ProximityDevice`](http://msdn.microsoft.com/en-us/library/windows/apps/jj207060%28v=vs.105%29.aspx)
* Phonegap: **no**
* FirefoxOS: **yes** [WebNFC](https://wiki.mozilla.org/WebAPI/WebNFC#Current_API)
* ChromeApps: **no**
* Tizen: **sysapp**

