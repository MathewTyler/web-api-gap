# In App Billing
Ability to pay quickly for digital goods from an app.

* browser: **partial** via HTML5 autocomplete, **alternative** via 3rd-party services
* sysapp: **browser**
* iOS: **yes** Apple Pay
* Android: **yes** [Google Wallet](https://developers.google.com/wallet/instant-buy/android/tutorial)
* Windows 8: **yes** [`CurrentApp.RequestProductPurchaseAsync`](http://msdn.microsoft.com/en-us/library/windows/apps/jj206950.aspx)
* Phonegap: **no**
* FirefoxOS: **yes** [MozPay](https://wiki.mozilla.org/WebAPI/WebPayment)
* ChromeApps: **yes** [`google.payments.inapp.buy`](https://developer.chrome.com/apps/google_wallet)
* Tizen: **no**

